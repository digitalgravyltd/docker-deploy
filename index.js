var http = require('http'),
    crypto = require('crypto'),
    Docker = require('dockerode'),
    docker = new Docker();

const KEY = process.env.KEY;
const PORT = process.env.PORT || 3030;
const ALGO = 'aes-256-ctr';


function encrypt(text){
  var cipher = crypto.createCipher(ALGO, KEY)
  var crypted = cipher.update(text,'utf8','hex')
  crypted += cipher.final('hex');
  return crypted;
}
 
function decrypt(text){
  var decipher = crypto.createDecipher(ALGO, KEY)
  var dec = decipher.update(text,'hex','utf8')
  dec += decipher.final('utf8');
  return dec;
}

function handleRequest(request, res) {
  var url = request.url.split('/'),
      image = url[1] + '/' + url[2],
      key = url[3];
  console.log('Image: ', image, 'Key: ', key);
  var decrypted = image === decrypt(key)
  console.log(decrypted);
  console.log(decrypt(key));
  console.log('----------------------------------');
  if(decrypted){
    res.setHeader('Content-Type', 'text/html');
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end('ok');
  }else{
    res.setHeader('Content-Type', 'text/html');
    res.writeHead(500, {'Content-Type': 'text/plain'});
    res.end('not ok');
  }
}

function handleListening(){
  console.log('Server listening on port:', PORT);
}

var server = http.createServer(handleRequest);
server.listen(PORT, handleListening);

docker.listContainers(function(err, containers){
  if(err){
    throw err;
  }else{
    containers.forEach(function(container){
      console.log('Image: ', container.Image);
      console.log('Key: ', encrypt(container.Image));
      console.log('----------------------------------');
    });
  }
});
